Unless otherwise stated today's show is released under a: Creative Commons, Attribution, ShareAlike, 3.0 License.

Timeline
 - 2007-02-23 https://creativecommons.org/2007/02/23/version-30-launched/
 - 2007-09-26 https://www.gnu.org/licenses/gpl-3.0.html
 - 2011-11-03 https://creativecommons.org/2011/11/03/copyright-experts-discuss-cc-license-version-4-0-at-the-global-summit/
    * Internationalization
    * Interoperability
    * Long-lasting
    * Data/PSI (Public Sector Information?)/Science/Education
    * Supporting Existing Adoption Models and Frameworks
 - 2013-11-25 https://creativecommons.org/2013/11/25/ccs-next-generation-licenses-welcome-version-4-0/
    * 30-day violation grace period  
      https://creativecommons.org/faq/#how-can-i-lose-my-rights-under-a-creative-commons-license-if-that-happens-how-do-i-get-them-back
 - 2013-12-06 16Z--18Z CC site moves to 4.0 -- last snapshot with 3.0 is https://web.archive.org/web/20131206155520/http://creativecommons.org/
 - 2014-10-21 CC-by-SA 4.0 and Free Art License 1.3 defined as two-way compatible licenses  
   https://creativecommons.org/2014/10/21/big-win-for-an-interoperable-commons-by-sa-and-fal-now-compatible/
 - 2015-10-08 GPLv3 defined as a one-way compatible license for CC-by-SA 4.0  
   https://creativecommons.org/2015/10/08/cc-by-sa-4-0-now-one-way-compatible-with-gplv3/

Other links
 - 39 jurisdiction ports of by-SA 3.0 https://wiki.creativecommons.org/wiki/CC_Ports_by_Jurisdiction
 - Next episode will be based on https://creativecommons.org/version4/
