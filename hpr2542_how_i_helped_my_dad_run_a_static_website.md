My [#hprep](https://social.heldscal.la/clacke/tag/hprep) tag up on Heldscalla serves as inspiration for times like this, when I should just record something while I have the chance. Suggest more topics for me to orate about and I'll put them up there!

In this episode I'm talking about how I've set up [SparkleShare](http://www.sparkleshare.org/) (web site currently down, try the [archived site](http://web.archive.org/web/20180320114552/https://www.sparkleshare.org/) if it's still down when you're reading this) and [GitLab Pages](https://about.gitlab.com/features/pages/) to allow my dad to tinker with a static web site locally on his machine and automatically get the changes up on the official URL without having to bother with any manual steps (at least on the happy path).

Errata: Oops, I said Jekyll uses Python. It uses Ruby.

TL;DL: We have two directories, two git repos. He doesn't have to know about git. He plays around in the staging directory first, looks at the test site how it turned out, when he's happy he just copies the files over to the production directory and they go live. SparkleShare automatically pushes to [gitlab.com](https://gitlab.com/) (I didn't say it outright in the episode, but yeah, I'm using the hosted service -- that's basically the point of this mode of doing things, minimal setup, responsibility and maintenance for me), and GitLab CI runs [Jekyll](https://jekyllrb.com/)  (use the static site generator of your choice) to copy files over for deploying, and finally  GitLab Pages deploys the new site.

I believe all of this took me less than two hours to set up, effective time, once I got around to it (and was in the same time zone as my dad's computer). Don't forget to add your verification TXT record in the DNS.
