{ pkgs ? import ./nix {}
, asciidoctor ? pkgs.asciidoctor
, pandoc ? pkgs.pandoc
, asciidocnames ? import ./asciidocnames.nix
}:

let
  asciidocnameToPath = docname: builtins.toPath "./${docname}.adoc";
  mergeSrcs = srcs: name: pkgs.runCommand name { inherit srcs; } ''
    mkdir $out
    for src in $srcs; do
      ln -s $src $out/''${src:${builtins.toString (34 + (builtins.stringLength builtins.storeDir))}}
    done
  '';
  asciidocnameToHtml = asciidocname: pkgs.runCommand "${asciidocname}.html" {
    buildInputs = [ asciidoctor pandoc ];
  } ''
    asciidoctor -o "${asciidocname}.docbook" -s -a showtitle -b docbook45 \
      "${./. + "/${asciidocname}.adoc"}"
    pandoc -f docbook -t html5 -o "$out" "${asciidocname}.docbook"
  '';
in
mergeSrcs (map asciidocnameToHtml asciidocnames) "asciidocs"
