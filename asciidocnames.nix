builtins.filter (s: (builtins.isString s) && s != "") (builtins.split "[ \n]+" ''
  hpr2562_laptop
  hpr2557_styx
  hpr2623_actors_and_agents
  hpr2793_bash_coproc
  hpr2802_mid-life_assessment
  hpr2807_bash_local
  hpr2812_5g_health
  hpr2817_success
'')
