Imagine that, instead of just reading the following words, you are listening to them from a video message that started playing on your computer, who knows why or where from. The message is spoken by a person with a deep voice, sun glasses and who might as well have in each hand a lively-colored pill. You can see the reflection of his hands and of the pills in his sunglasses. He says something that goes like this:


    Greetings, inhabitants of the bubble,

    Millennia ago, some humans found out that they lived on the surface of a nearly-spherical bubble that wandered in sidereal space. For over ten centuries, this truth has been concealed from most of you. When you rediscovered it, the powerful of those days attempted to deny it and to suppress it. They almost did it...

    Fortunately, now, we know where we live. But how about you?

    Many of you still believe you live on this same surface, in the beginning of the 21st century, in an era defined by your technology. It's an illusion: today, there are more than a billion humans living in a bubble in cyberspace. The filters that create this illusory world are controlled by machines. They keep you all prisoners to turn you into... products!

    It is through electronic communication devices that you access most of this illusory reality. Controlling these devices, it is easy for machines to create this illusion and control you. How many of you really believed Trump was the least bad option for the USA? How many believed Brexit was a good idea for the United Kingdom? How many in Brazil would bet Aécio was honest? How many regretted these choices, very often right after voting? Do you realize the illusions that were built and that, shortly thereafter, when they no longer mattered, fell apart?

    I can't explain why the machines allowed these huge delusions to be observed. I can't even understand why so many of you keep on believing the sources of these illusions, even after realizing the realities built by them were false. I don't even know how I managed to make this message reach you, if it really will! But if it did, now you know about the illusions, so I will let you know how your world turned up like this.

    Long before the Bubbletrix was built, there was a man who could change any program in the computers he used, so that they'd behave as he wished. It was he who created the GNU Resistance and freed the first of us. It was he too who taught us the truth: as long as the Bubbletrix exists, the human race will never be Free.

    Other humans created programs that controlled others' computers to serve them. They trained humans to find it normal that a computer does not obey its user, that it fools her, that it keeps her data hostage.

    Before that, others had already created programs that, in a far more subtle way, also controlled humans. Not computer programs, but of much older devices called TV sets. They were a bit like computer displays, but configured to receive signals only from some privileged broadcasters. They broadcast essentially illusions: some were acknowledged as fiction, others attempted to pass for reality, and others had as their primary purpose to distort it. They all had as main goal to distort human behavior and capture their attention and their time, to sell them to advertising machines.

    The idea of selling human attention was brought to computer networks. It started with information search services. To target ads and increase the selling price of human attention, data on each used human started being collected separately. It was the beginning of the end of human privacy.

    Many other services were created, not with the goal of helping humans, but rather of seducing them into turning over their data, to further increase the value of their attention to advertisers.

    The machines in charge of these services, gatherings and analyses covered the entire planet with a huge cloud. They turned most of humankind dependent on them for the simplest tasks. Humans grown and enslaved by them became mostly incapable of surviving without this cloud coverage. They remained linked to the ad machines' network through cellular connectors, that granted them nearly direct access to human neurons.

    They allowed installation, in their cellular connectors, of programs they could not control, and that made the collection of their data, the diffusion of illusions and control of personal behavior even easier.

    So far, humans at least seemed to get something of value back in exchange for their attention and data. But once behavior control sets in, it snowballs and machines don't need to offer anything of value any more. Humans under remote control don't hesitate to give up to communication intermediaries even their most intimate data, without any payoff whatsoever. Thus, machines start knowing so well the heads in their human farms, grown for data extraction, that machines could select not just ads, but also third-party private messages that humans would, or rather should, see, according to machines' interests.

    This is how Bubbletrix started: by molding minds through the selection of ideas exposed to them, the illusory reality was formed. In this illusion, it is believed, without questioning, that the cloud, the computers and the programs are beneficial to their users. Come to think of it, they really are. But you are not the users. You are useds. The real users are those who designed these systems to gather data, spy, monitor and control you, to know the best moment to show you an ad for a product you might want to buy, or news that plants in your mind an idea the machines wish to bloom.

    So-planted ideas become so strong in the minds of businessmen, teachers, lawyers, carpenters, beautiful models in red dresses and so many other Bubbletrix inhabitants that they become part of the system. They become so hopelessly dependent on the system that they will even fight to protect it. But it's precisely for the liberation of these people that we fight. So they end up fighting against their own freedom.

    Now, aware of this all, each one of you is going to have to make a choice. I can't make blue or red pills get to you, but they'd be just symbols for the actual choices. What you may or may not take into your own hands is your future, your fate, your freedom. If you choose to keep on living in the illusory world of the Bubbletrix, being controlled, monitored and manipulated all the time, having your communications that should be private intruded in by the machines... I'm sorry, but I wish your illusions be far juicier and more delicious than those I've been told about.

    Now, if you wish to join the GNU Resistance and live in the LibrePlanet, come to the Free side of the Cyberspace:

    Use Free and decentralized communication networks, like GNU social/StatusNet/Mastodon, Diaspora and Pump.io, or distributed, like Twister, instead of the centralized manipulators. Don't even think of leaving your friends behind: bring them with you!

    Free your mind! Understand well the dangers of the cloud, of proprietary software and of the ad machines to be really sure you want to leave them behind. Understand that leaving them behind is not a sacrifice: that who understands the value of freedoms of speech, of expression, of communication, of software, of assembly, of association, of movement, knows that the real sacrifice would be to give them up.

    Install Free Software on your own computers so that they stop controlling you: the GNU operating system with the Linux-libre kernel works on nearly any kind of computer, from mainframes to laptops and even some tablets and cell phones. There are thousands of Free apps ready to work on each of them. For some tablets and cell phones, there's also Replicant, that's not quite a GNU, but it's a genuinely Free Android.

    If you find out your computing device is not compatible with 100% Free Software, there are many ways to start treading the path to freedom, be it with Free apps while still on freedom-depriving systems, be it on quasi-Free systems, that use freedom-depriving components to enable the use of a lot more Free Software on freedom-incompatible computers. Just don't conflate the first step with the end goal: the less-devastating drug used for harm reduction is still a drug that causes dependency and harm.

    Put aside the nebulous apps that capture your data, and go back to keeping your data on your own computers. If you must use network apps, or network data storage, build and use your own cloud, with FreedomBox and ownClowd or Nextcloud. While at that, run on them your own nodes in the Free decentralized communication networks!

    Our communities are always open to Bubbletrix refugees, and we offer help in many ways. Look for us in the Free communication networks, while machines have not yet figured out how to block them. We will be there to help you find your escape route.


 	
