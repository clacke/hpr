In this episode I'm adding some input methods to a standard Ubuntu 20.04 install.

First I go to the _Language Support_ window. Either search (press Super/Windows, then type) for
"Language Support" and go there directly, or search for "Region & Language", go there, then click
"Manage Installed Languages".

There I click _Install / Remove Languages ..._, check the Chinese I want, then click _Apply_.

Then I add the packages for the input methods I want, either using the command line (`apt install`)
or the _Ubuntu Software_ application:

- Chinese Quick: `ibus-table-quick-classic`
- Chinese Pinyin: `ibus-libpinyin`
- Chinese (Cantonese) Jyutping: `ibus-table-jyutping`

Bonus input method:

- Unicode emoji: `ibus-typing-booster`

Now for each user that wants an input method, I search for and go to _Region & Language_. At the
bottom of the _Input Sources_ section I click the plus button. The different input methods are found
by clicking down into different sublists. Click the right choice, then _Add_:

- Chinese Quick: _Chinese_ -> _Chinese (QuickClassic)_
- Chinese Pinyin: _⋮_ -> _Other_ -> _Chinese (Intelligent Pinyin)_
- Chinese (Cantonese) Jyutping: _Chinese_ -> _Chinese (Jyutping)_
- Unicode emoji: _⋮_ -> _Other_ -> _Other (Typing Booster)_
- EurKEY: _English (United States)_ -> _EurKEY (US based layout with European letters)_

The EurKEY layout is part of the standard English language support in Ubuntu.

Finally, to enable unicode color emoji as completion suggestions, switch to the Typing Booster mode
by pressing Super/Windows+Space repeatedly until you see the rocket in your notification area. Click
the rocket -> _Unicode symbols and emoji predictions_ -> _On_.

I owe you a followup episode once I figure out how to make this work for Guix applications running
inside Ubuntu.

----

This episode was based on a Fediverse thread:  
https://libranet.de/display/0b6b25a8-6760-517c-52c9-654926232346

References for your further study:

 - [IBus](https://en.wikipedia.org/wiki/Intelligent_Input_Bus) is an input method protocol for the
   GNU/Linux desktop. Other protocols supported are [XIM](https://en.wikipedia.org/wiki/Xim),
   [SCIM](https://en.wikipedia.org/wiki/Smart_Common_Input_Method),
   [fcitx](https://en.wikipedia.org/wiki/Fcitx) and [uim](https://en.wikipedia.org/wiki/Uim). You
   have to choose one of these to use for all your input methods, but the most common input methods
   exist at least for IBus and the first three of the rest, so this is not as much a limitation as
   it sounds like.
 - Quick is a simplification of the
   [Cangjie input method](https://en.wikipedia.org/wiki/Cangjie_input_method). Cangjie assigns
   radicals, character components, to 24 keys on the alphabetic keyboard, and you combine these into
   a character. In Quick you combine two and then choose a completion from a list.
 - [Pinyin](https://en.wikipedia.org/wiki/Pinyin) is a romanization, a Latin alphabetic spelling,
   for [Standard Chinese](https://en.wikipedia.org/wiki/Standard_Chinese) (Mandarin).
 - [Jyutping](https://en.wikipedia.org/wiki/Jyutping) is one of
   [many](https://en.wikipedia.org/wiki/Hong_Kong_Government_Cantonese_Romanisation)
   [romanizations](https://en.wikipedia.org/wiki/Yale_romanization_of_Cantonese)
   [for](https://en.wikipedia.org/wiki/Sidney_Lau_romanisation)
   [Cantonese](https://en.wikipedia.org/wiki/Cantonese_Pinyin).
 - [Guix](https://guix.gnu.org/) is a GNU/Linux OS and also a package manager that can be installed
   and coexist with the GNU/Linux OS you already have, allowing you to mix and match programs from
   both sources. See also [hpr2198](https://hackerpublicradio.org/eps.php?id=2198) and
   [hpr2308](https://hackerpublicradio.org/eps.php?id=2308).
