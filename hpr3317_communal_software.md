Three good decades ago, Richard Stallman founded the free software movement and gave it a name.

Two good decades ago there was a fork and Eric S. Raymond, Bruce Perens and others founded the open
source software movement, and neglected to tell us who gave it a name.
(it was Christine Peterson[[0]](#footnote0))

Ever since then, the free software side of the two movements has been careful to guard the boundary
between the two, see Richard Stallman's essay "_Open Source Misses the Point_".[[1]](#footnote1)

But lately a lot of people have increasingly been feeling that _free software_ misses the point.
Ironically a lot of this has been coming from the _open source_ side of things, as the official free
software philosophy has been firmly anchored with Stallman, and he hasn't been interested in moving
his philosophy in more inclusive directions.

For sure, there are a lot of people in free software who have been wanting to go in this direction
as well. I've been thinking of it as a "free software plus", as it builds on the free software
philosophy, but adds aspects of social responsibility. The fact that Stallman was forced to resign
from being Free Software Foundation president two years ago was a sign that people inside free
software cared about more than just the code and what freedoms it gives the recipient.

A month ago, if you are listening to this on April the 20th 2021,
a manifesto was published called "_Towards A Communal Software Movement_", and I'll get to
that in a minute. I mentioned the names of the drivers of the previous movements, but this author
has said "I intentionally left authors' names out of it"[[2]](#footnote2), and I think that makes sense. Part of the
problems with previous movements has been this Great Man of History fallacy, which may have kept
them focused and on track, but it has also held them back.

The movement is young and has already changed names once as I was writing about it. The manifesto is
now "_Towards A Cooperative Technology Movement_", and I have updated the shownotes and my commentary
to reflect that.

https://misskey.de/notes/8k0igd5tcd

I see the difference between free software and cooperative technology similarly as the difference
between open source and free software.

There are certainly people within open source and on the Open Source Initiative board that look
further than just the license, and treat open source like just another brand name for free software.

But at its core, the Open Source Definition is all about the licensing and that document is the
shared common ground for all open source. People write code for different reasons and there's a
license and contribution model that allow them to come together without those differences of purpose
getting too much in the way.

So if the software and the license is "what" we're building, the philosophical documents of free
software provide the guidance on "why" we are building it: We want to get away from proprietary
software, we want to control our own computing, we want the freedoms to use, learn, modify and
share, etc. Free software is about our freedoms.

So just like "free" is right there in the name, maybe the "community" in "communal software" or
the "cooperative" in "cooperative technology" is
all about the "who": Who gets the freedom, who has the influence, who is affected.

And again, lots of people in free software do care about community principles beyond code, care about
social responsibility, but the shared baseline is the care for formal, technical and individual user
freedom: If _you_ receive the code, _you_ are allowed the _technical rights_ to update the code, the
code or license should not restrict _your_ freedoms, you, the recipient of the software, the hacker,
the code contributor. It says nothing about _practical_ user freedom and it says nothing about the
community beyond the immediate user.

----

That was my commentary. Now let's read the manifesto.

https://cooperativetechnology.codeberg.page/

----

Before I saw the manifesto, I had written a draft list of aspects beyond licensing and contribution
that determine the social good of your project:
https://libranet.de/display/0b6b25a8-3060-61f6-28df-cae554943983

The conversations that led directly to the creation of the manifesto:
https://social.polymerwitch.com/@polymerwitch/105934078911643041
https://fosstodon.org/@be/105952735879246194

<a name="footnote0">[0]</a> https://opensource.com/article/18/2/coining-term-open-source-software

<a name="footnote1">[1]</a> https://www.gnu.org/philosophy/open-source-misses-the-point.en.html

<a name="footnote2">[2]</a> https://fosstodon.org/@be/105952960559032774

----

# Towards A Cooperative Technology Movement

In response to the surprise, undemocratic reinstatement of Richard Stallman to the board of directors of the Free Software Foundation after his resignation in September 2019, the Free and Open Source Software movement is in the midst of a reckoning.
The authors of this document recognize and honor the contributions Richard Stallman has made to this movement while unequivocally condemning his harmful behavior which has pushed many capable, dedicated people away from the movement.

Regardless of what happens in the Free Software Foundation, we believe it is time to reflect on the shortcomings of our advocacy so we can grow into a more effective and inclusive movement for justice.
Towards this end, we believe the movement will benefit from new terminology to describe what we do and what we aim for.
Richard Stallman authored the [free software definition](https://gnu.org/philosophy/free-sw.html) in 1986.
This term has always created difficulties communicating the ideas behind it because of the different meanings of the word "free" in English.
Moreover, it is not the freedom of machines we are concerned with, but the freedom of humans.
In response to this and other issues, in 1998, the term [open source](https://opensource.org/docs/osd) was promoted using an adapted version of the [Debian Free Software Guidelines](https://www.debian.org/social_contract#guidelines).
The history of computing in the past 23 years have validated critiques that the term "open source" is insufficient for communicating the values behind it.
The term "open source" and the ecosystem of Free and Open Source Software (FOSS) is today used by powerful companies, governments, and other institutions to harm people on enormous scales through [surveillance](https://www.youtube.com/watch?v=0hLjuVyIIrs) and [violence](https://www.theverge.com/2019/10/9/20906213/github-ice-microsoft-software-email-contract-immigration-nonprofit-donation).
These institutions use FOSS to minimize economic costs by benefitting from decades of work done by others, much of which was done by unpaid volunteers motivated by curiosity, passion, and the ideals of the FOSS movement.

We believe a significant reason for the failures of both "free software" and "open source" to prevent this cooptation is that the men who coined and initially promoted these terms did not and do not critique capitalism.
Richard Stallman has generally [dodged](https://www.gnu.org/gnu/byte-interview.html) [the](https://www.gnu.org/gnu/manifesto.html#competition) [question](https://www.gnu.org/philosophy/bill-gates-and-other-communists.en.html) of whether free software is opposed to capitalism.
In the historical context of the United States in the 1980s, that may have been a wise decision.
But that was then, and now it is 2021.
The promoters of "open source" [emphasize its compatibility with capitalism](https://www.youtube.com/watch?v=4vW62KqKJ5A&t=2186s) and [go out of their way to distance "open source" from critiques of capitalism](https://www.youtube.com/watch?v=4vW62KqKJ5A&t=3824s).
We believe we need to build on the FOSS movement with an explicitly anticapitalist political movement which proactively collaborates with other movements for justice.

We propose the term "cooperative technology" for this movement.
By "cooperative technology", we mean technology that is constructed by and for the people whose lives are affected by its use.
While this builds on the Free and Open Source Software movement, we aim to apply the same principles to hardware as well, although the criteria by which we evaluate hardware and software will of course not be identical.
It is not sufficient to narrowly focus on the people who directly interact with computers.
Cooperative software which is run on a server should not be controlled solely by the administrator of the server, but also by the people who interact with the server over a network.
Similarly, the data generated by the technology and the data which it requires to function should be in the control of the people who are affected by the technology.
Cooperative software that uses cameras should not be controlled solely by the people who own the cameras, but also the people who are observed by the cameras.
Cooperative electronic medical record systems should not be designed for the interests of insurance companies or hospital administrators, but for the interests of patients and the clinicians who directly use it.

We aim for a world in which all technology is cooperative technology and recognize that any amount of proprietary technology is in conflict with this goal.
As an anticapitalist movement, we recognize that any institution which motivates people to put money, power, or self-interest above the welfare of humans is in conflict with our goals.
Corporations are beholden to their shareholders who can hold the corporation legally liable for spending money in a way that is not intended to further enrich the shareholders.
Other capitalist forms of enterprise have similar problems, incentivizing the profit of an elite few over the impact their activities have on others.

We are not opposed to exchanges of money being involved in the creation or distribution of software or hardware.
However, we should carefully consider the motivational structures of the institutions which fund technology development.
Who benefits from the technology and who determines the priorities of its development and design?
These are questions we ask about technology whether money is involved or not.
It is in our interest to use safeguards to ensure that technology always remains controlled by the community which develops and uses it.
Copyleft is one such safeguard, but it is insufficient on its own to prevent cooptation of our movement.
Any cooperative technology project that receives funding from a for-profit enterprise must institute governance structures which prioritize community interests over profit in case there is a conflict between the two.
We oppose business models which are in conflict with community interests such as ["open core"](https://media.libreplanet.org/u/libreplanet/m/why-i-forked-my-own-project-and-my-own-company-31c3/)/[proprietary relicensing](https://sfconservancy.org/blog/2020/jan/06/copyleft-equality/).

Similarly, we are opposed to authoritarian and hierarchical governance structures of technology projects such as "benevolent dictators for life".
Cooperative technology is developed democratically; no single individual should have ultimate authority in cooperative projects.
While we recognize the need for leadership and private communication, discussions regarding cooperative technology should take place in public unless there is a specific reason for communications to be private.
Organizations which advocate for cooperative technology should likewise operate democratically and transparently.

We recognize that creating high quality technology requires much more than engineering skills.
Cooperative technology is not only for people who have the skills of writing code (unless the software is for writing code such as a compiler) nor the skills to design hardware.
Cooperative technology strives to be easy to use, including for people with disabilities, and acknowledges that this is best accomplished by continual dialog between engineers and users.
Providing such feedback is a valuable way to contribute to the construction of cooperative technology without needing engineering skills.
Ideally, the engineers of the technology should also be using it themselves.
Moreover, there are many ways to contribute to cooperative technology without programming skills such as imagining ideas for new features, reporting bugs, writing documentation, graphic design, translation, promotion, and financial support.

The free software movement has [failed to create a world in which humans in technological societies can live without using proprietary software unless one chooses to live the ascetic lifestyle of Richard Stallman](https://archive.fosdem.org/2019/schedule/event/full_software_freedom/).
Expecting people to not use any proprietary technology and judging people for not meeting this standard pushes people away from our movement.
People who are coerced into using proprietary technology deserve our empathy and invitation into our movement, not condescension.
Let us criticize institutions which pressure people into using proprietary technology, not the people who choose to use it.
To that end, we strive to use cooperative technology tools as much as possible in our efforts to build cooperative technology.

The purpose of this document is *not* to proclaim a legalistic set of criteria for determining what technology is cooperative and what technology is not.
History has demonstrated that this is not an effective political tactic for the reasons explained above.
The [free software definition](https://gnu.org/philosophy/free-sw.html) and the [open source definition](https://opensource.org/docs/osd) are useful criteria for evaluating copyright licenses for code, but an effective political movement cannot be so narrowly focused on legalistic and binary judgements of copyright licenses to judge whether certain technology aligns with our goals.
We believe the focus of the cooperative technology movement should be on the practical impacts that the use of technology has on humans and the universe we inhabit.
The scope of this extends beyond humans and must consider the [environment around us](https://www.bbc.com/news/technology-56012952).
Moreover, we believe it is counterproductive to have a small self-appointed group of privileged men determine what our movement's terminology, goals, and tactics are.
We encourage anyone interested in building a better world through technology to engage in discussions with your own communities about what you want "cooperative technology" to mean.

While we agree with the Ethical Software Movement that we must resist when our efforts are coopted for unjust purposes, we reject putting restrictions on the ways people may use software through copyright licenses as a wise tactic for achieving our goals.
The history of the Free and Open Source Software movement has shown that the proliferation of incompatible copyright licenses which prohibit software from being legally combined creates more obstacles than opportunities for our movement.
Any new copyright licenses for use with cooperative software must be written with this consideration in mind to intentionally avoid fracturing the software ecosystem.
Adopting incompatible copyright licenses for different software would make it easy for our adversaries to divide and suppress the movement.

Language is constructed collectively and is always evolving.
It is counterproductive to our movement to refuse to collaborate with people because they use the words "open source" or "free software" to describe their work.
They may even disagree with the entire premise of this document.
That does not mean we should not work together towards shared goals, but we should be conscious that our goals may not perfectly align and this may cause tension in our communities from time to time.
We invite anyone to collaborate with us who is interested in building a better world and treats us and others in our communities with dignity and respect.

This document is licensed under the [CC0 license](https://creativecommons.org/publicdomain/zero/1.0/). Contributions are welcome on [Codeberg](https://codeberg.org/CooperativeTechnology/website). If you disagree with parts of this, feel free to fork it and say what *you* want to say.
