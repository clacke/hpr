{ pkgs ? import ../nix {}
}:

with pkgs;

{
  build = mkShell {
    buildInputs = [ jekyll.gems.kramdown ];
    shellHook = ''
      kramdown hpr3072_py_dev.md > hpr3072_py_dev.html.new && mv hpr3072_py_dev.html{.new,}
      exit 0
    '';
  };
}
