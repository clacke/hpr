Styx -- The Purely Functional Static Site Generator
---------------------------------------------------

For the Fractalide web site, we are using Styx as a site generator. Here's a bit of how and why.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

I switched phones, and complained about the microphone. It probably made a greater difference that I was recording in 16 kHz Vorbis, because I was on a fresh install of Audio Recorder. Always double-check your settings, and apologies for the quality.

'''

I am currently in the process of converting https://github.com/fractalide/fractalide-website-styx/[our website^] from https://gohugo.io/[Hugo^] to Styx.

https://styx-static.github.io/styx-site/[Styx^] is s static site generator written entirely in the https://nixos.org/nix/[Nix^] language. It is able to figure out exactly what pages need to be rebuilt depending on what you changed in your page source and data sources, and all intermediate results are stored in the Nix store.

The parsing of http://www.methods.co.nz/asciidoc/[AsciiDoc^] and http://fletcherpenney.net/multimarkdown/[(multi)Markdown] is done by external tools, but the templating and layouts is all Nix.

I thought I may have dreamed the bit about https://nest.pijul.com/pmeunier/nix-rust[carnix^] or https://github.com/NixOS/nixpkgs/blob/master/pkgs/build-support/rust/default.nix[buildRustPackage^] parsing TOML within Nix, because I couldn't find any evidence of them ever having done that. But then I discovered it was in https://github.com/mozilla/nixpkgs-mozilla/blob/master/lib/parseTOML.nix[nixpkgs-mozilla^] I had seen it! That's Mozilla's overlay for nixpkgs, which makes Rust Nightly always available in Nix, so it's kind of Nix's https://www.rustup.rs/[rustup^] equivalent. So yeah, I guess I had dreamed who did it, but not that _somebody_ did it. :-)
