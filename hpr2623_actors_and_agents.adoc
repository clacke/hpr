2018-07-29 HPR Actors and Agents, Sprites and Fractals

In which I sit down with Christopher Lemmer Webber and try to keep it short, but end up with an hour of Actor Models, Flow-Based Programming, Object Capabilities, ActivityPub and more

Plugs:

 * Chris works on https://w3c-ccg.github.io/ocap-ld/[Object Capabilities for Linked Data] (OCAP-LD) and other things for a living, at https://digitalbazaar.com/ .
 * clacke works at https://fractalide.com/ making better tools for future programmers.
 * We're both fortunate enough to have the chance to get paid for creating all Free Software.
 * Christopher Lemmer Webber and Morgan Lemmer Webber will be speaking at https://con.racket-lang.org/[RacketCon 2018] on the topic _Racket for Everyone (Else)_, how non-programmers can do "programmable publishing" using Scribble when writing humanities papers, and how Racket could better target not just beginner programmers and hard-core language theorists, but also the huge space in between.
 
More information on the topics covered:

 * For my last show with Chris, see http://hackerpublicradio.org/eps.php?id=2198[hpr2198 :: How awesome is Guix and why will it take over the world]
 * His Actors library for Guile Scheme is https://www.gnu.org/software/8sync/[8sync]. A video of him playing in front of an audience with the Multi-User Dungeon (MUD) on top of 8sync is available on the front page.
 * Spritely, the media sharing platform that may or may not be the next MediaGoblin, is currently vaporware, but the underlying https://gitlab.com/spritely/goblins/[Goblins] Actors library for Racket is real and works.
 * Wikipedia has more on the https://en.wikipedia.org/wiki/Actor_model[Actor Model], https://en.wikipedia.org/wiki/Flow-based_programming[Flow-Based Programming] and https://en.wikipedia.org/wiki/Object-capability_model[Object Capabilities] (OCap).
 * We also mentioned in passing https://en.wikipedia.org/wiki/Communicating_sequential_processes[Communicating Sequential Processes].
 * I knew that OCap grew up in the context of https://en.wikipedia.org/wiki/E_(programming_language)[E], but I didn't know that E itself actually grew out of the needs of a form of MUD, built by Electric Communities (EC). I'm guessing this is the graphical MMORPG https://en.wikipedia.org/wiki/Habitat_(video_game)[Habitat] that EC built for Lucasfilm back in 1986, for the Commodore 64. Some writing about EC and the philosophy and experience around what they did is collected at http://www.crockford.com/ec/.
 * https://en.wikipedia.org/wiki/Language-oriented_programming[Language-oriented programming] (LOP) is an old LISP methodology: Understand the problem, write a language for describing and solving the problem, write the solution in that language. Racket (itself a LISP) is heavily focused on this, and comes with a whole slew of languages out of the box. The Racket slogan on http://racket-lang.org/ is "solve problems -- make languages".
 * https://cacm.acm.org/magazines/2018/3/225475-a-programmable-programming-language/fulltext[A recent ACM article] describes in depth what the challenges of good LOP are, and how Racket helps the programmer work with it.
