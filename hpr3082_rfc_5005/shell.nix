{ pkgs ? import ../nix {}
}:

with pkgs;

let buildScript = writeScript "buildHtml.sh" ''
  #!${bash}/bin/bash
  [ -n "$1" ] && printf "$1"
  ${jekyll.gems.kramdown}/bin/kramdown hpr3082_rfc_5005.md > hpr3082_rfc_5005.html.new && mv hpr3082_rfc_5005.html{.new,}
  [ -n "$2" ] && echo "$2"
''; in

{
  build = mkShell {
    shellHook = ''
      exec ${buildScript}
    '';
  };
  watch = mkShell {
    buildInputs = [ entr coreutils ];
    shellHook = ''
      exec entr ${buildScript} rebuilding... done <<< hpr3082_rfc_5005.md
    '';
  };
}
