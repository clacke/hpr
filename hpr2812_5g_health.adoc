Is 5G mobile data a danger to your health?
==========================================

Apply Betteridge's Law of Headlines to find out the answer
----------------------------------------------------------

This is mostly verbatim from my Fediverse post https://libranet.de/display/0b6b25a8-165c-9c7f-b55d-c7a077813050[], with a few minor edits.

The anti-5G campaign has been cooking for many years now, and at the epicenter of it all are two men, Lennart Hardell and Rainer Nyberg. It's a Swedish-Finnish phenomenon that is now really making the rounds and spreading internationally, as actual commercial deployment of 5G networks draws nearer.

As a Swede, I apologize. These two do not represent the Swedish or Finnish cancer or radiation research community, and our media have given them far more space in the public discourse than their work merits.

They are heavily quoted in networks of pseudoscience, including anti-vaccine sites, right-wing "alternative facts" sites and Strålskyddsstiftelsen ("Swedish Radiation Protection Foundation"), a private foundation created in 2012 with a deceptive name meant to invoke authority, which has had to be corrected on multiple occasions by the actual Swedish Radiation Safety Authority, Strålskyddsmyndigheten.

Strålskyddsstiftelsen received the 2013 "Misleader of the Year" award from the main Swedish scientific skeptics' society, Vetenskap och Folkbildning ("Science and Public Education") for "[their fearmongering propaganda and biased reporting on the health effects of mobile telephony use and wireless networks]".

https://www.vof.se/utmarkelser/tidigare-utmarkelser/aretas-forvillare-2013/ (in Swedish)

These networks are part of a feedback loop where they get media attention, politicians pick up on their claims and use them to invoke the precautionary principle and get precautionary regulation in place, or judges rule based on the claims, which then gets quoted by these entities as evidence that they were right all along.

They make it very hard to find factual information on whether millimeter-wavelength radiation actually has any different effect from the centimeter-wavelength radiation that we have been using for over two decades without any documented harmful effects, because wherever you look you just find these sites claiming that we have definitely had adverse health effects for the last two decades and the new frequency bands will definitely be far worse.

When you dig deeper into the claims on these sites you find a handful of cherry-picked articles, leading back to the two men mentioned at the top, to studies with flawed methodology like self-reported surveys on mobile telephony use among cancer patients, or to the pseudoscience/media/politics/law feedback loop. And it's all about centimeter waves, which simply have shown no conclusive sign of increasing brain cancers or any other adverse health effect related to the radiation. For every positive report made you can find one that reports brain cancer fell as we introduced mobile phones. There is a massive body of data, and if the signal were there, we would have seen it by now.

I'm no cancer researcher, but neither is Rainer Nyberg, he's a retired professor in pedagogy. He's a concerned citizen. https://en.wikipedia.org/wiki/Lennart_Hardell is an actual oncologist and professor who has studied carcinogens, but his research results on the wireless/cancer connection have been dismissed as "non-informative", "post hoc", "barely statistically significant" and "flawed" by his peers. There is nothing there.

We know that high-voltage 16.7 Hz fields increase the risk for leukemia in train drivers, but we don't know why. I am open to the possibility that 20-50 GHz waves have different consequences from 2 GHz waves, but I'd have to hear it from a credible source.

Straight up DNA mutation is out the window, and that's one of the centerpoints of these campaigns. This is still frequencies below visual light, it's not ionizing radiation. No plausible mechanism has been suggested, and there is no clear data on any adverse effects.

We use millimeter waves for the full body scans in US airports. Surely the effects of those have been studied? The top search results go to truthaboutcancer and infowars and similar names I won't even bother to click. I don't want to read another article about how all cancer research after 1950 has been wrong, we should all just eat chalk to balance our acidity, and cancer is a fungus.

Apart from the pseudoscience sites I found one paper on the first search results page, concluding that X-ray backscatter scanners have well-known risks, but radiation levels are far below safety standards, both for passengers and for security staff, and also below the background radiation exposure while flying, and millimeter-wave scanners, while an "alarmingly small amount of information about its potential health effects" is available, "The established health effects associated with non-ionizing radiation are limited to thermal effects" and "these scanners operate at outputs well below those required to produce tissue heating", that is, we currently don't know of a way millimeter waves might be harmful: https://www.sciencedirect.com/science/article/pii/S1687850714000168 (https://doi.org/10.1016/j.jrras.2014.02.005)

For a guide on how to spot pseudoscience and how to read scientific papers, see ahuka's excellent http://hackerpublicradio.org/eps.php?id=2695[hpr2695: Problems with Studies].


https://en.wikipedia.org/wiki/Betteridge%27s_Law_of_Headlines