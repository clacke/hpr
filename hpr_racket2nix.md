This time, it's a triple whammy! It's functional programming, functional package management *and* soundscape!

Listen to me walk through five shopping malls and one bicycle tunnel, as I rant about how flow-based programming microservices and functional package management will save the future of programming and software reuse, and usher in a new era of software quality and productivity!

If it sounds like I'm a bit down about working alone on `racket2nix`, you're interpreting me wrong! I didn't expect any feedback at all from the small to non-existent racket/nix intersection, but it turns out the intersection is larger than I though, and I'm grateful for any words of encouragement, and feedback in any form on what the community needs.

Links:

 - [racket2nix](https://github.com/clacke/racket2nix-clacke)
 - [racket](https://racket-lang.org/)
 - [nix](https://nixos.org/nix/)
 - [fractalide](http://fractalide.com/)
 
Nix is the mother of Guix:
 - [hpr2198 :: How awesome is Guix and why will it take over the world](http://hackerpublicradio.org/eps.php?id=2198)
